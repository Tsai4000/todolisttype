# How to use

## Pull
``` 
git clone https://Tsai4000@bitbucket.org/Tsai4000/todolisttype.git 
```

## Install 
``` shell 
npm install 
```
- You will need a .env file
```
DB_HOST=localhost
DB_PORT=3306
DB_USER=root
DB_PASS=root
DATABASE=todo_type
NODE_ENV=DEV
SECRET='s0e1c2r3e4t5'
SERVER_PORT=3000
```

## Run in TypeScript
- SQL init
```shell
npm run initSQL
```
- Run directly with TypeScript  
```shell 
npm run start
``` 

## Run in JavaScript
- Build js files 
```shell
npm run build 
```
- Copy views into dist

- Run server
```shell
npm run server 
```

# For Docker 
## Install 
- Install first
``` shell 
npm install 
```
- .env file for Docker
```
DB_HOST=mysql-container
DB_PORT=3306
DB_USER=root
DB_PASS=root
DATABASE=todo_type
NODE_ENV=DEV
SECRET='s0e1c2r3e4t5'
SERVER_PORT=3000
```
## Build JS
- Build
```shell
npm run build
```
## Build Image
- Build image
```shell
docker build -t todo .
```
- Run image 
  - mysql-container = Your mysql container name
  - port = Your output port
```
docker run -dit --name todolist --link  {mysql-container}:mysql-container -p {port}:3000 todo
```




