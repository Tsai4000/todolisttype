# Run "npm run build" in local first

FROM node:14-slim
RUN mkdir ~/tsapp
WORKDIR /root/tsapp
COPY package.json /root/tsapp/package.json
RUN npm install --production=true
COPY /dist /root/tsapp/dist
COPY /src/views /root/tsapp/dist/views
COPY .env /root/tsapp/.env

EXPOSE 3000

CMD ["npm", "run", "server"]