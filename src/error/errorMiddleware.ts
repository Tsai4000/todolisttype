import *  as Errors from './errors'
import { ErrorRequestHandler } from 'express'

const errorMiddleware: ErrorRequestHandler = (err, req, res, next) => {
  if (err instanceof Errors.GeneralError) {
    res.status(err.getCode()).json({
      status: 'error',
      message: err.message
    })
    next()
  }

  res.status(500).json({
    status: 'error',
    message: 'Server Error'
  })
  next()
}


export { errorMiddleware }