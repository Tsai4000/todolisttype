declare namespace Member {

  interface createValue {
    account: string,
    passwd: string,
    salt: string
  }

  interface updateValue {
    account?: string,
    passwd?: string,
    salt?: string
  }

  interface returnValue {
    account: string
  }
}
