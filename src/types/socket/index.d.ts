declare type Socket = import('socket.io').Socket
declare interface SocketDecodedBody extends Socket {
  decoded?: any
}
