declare interface router {
  method: 'get' | 'post' | 'put',
  path: string,
  beforeAll: Handler.middleware[],
  handlers: Handler.middleware[],
  afterAll: Handler.middleware[]
}