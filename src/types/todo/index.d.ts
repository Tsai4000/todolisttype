declare namespace Todo {
  interface createValue {
    subject?: string
    brief?: string
    level?: string
    author?: string
    content?: string
  }
  interface updateValue {
    subject?: string
    brief?: string
    level?: string
    author?: string
    content?: string
  }
}