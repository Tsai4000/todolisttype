import express from 'express'
import dotenv from 'dotenv'
import cookieParser from 'cookie-parser'
import path from 'path'
import * as hbs from 'hbs'

dotenv.config()
const app = express()

app.set('secret', process.env.SECRET)
app.use(express.json())
app.use(express.urlencoded({
  extended: false,
  limit: "1mb",
  parameterLimit: 10000
}))
app.use(cookieParser())

app.engine('html', hbs.__express)
app.use(express.static(path.join(__dirname, "../views")))
app.set('views', path.join(__dirname, '../views'))
app.set('view engine', 'html')

export { app }