import { createServer } from "http";
import { Server, Socket } from "socket.io";
import { app } from './express'

const socketServer = createServer(app);
// const io = new Server(socketServer, { cors: {} });

// io.on("connection", (socket: Socket) => {
//   console.log('conn', socket.id)
// });

// const todoUpdateEmitter = (data: Todo.updateValue) => {
//   io.emit('todoUpdate', data)
// }

export { socketServer }