import dotenv from 'dotenv'
dotenv.config()

const _type = process.env["NODE_ENV"] || "DEV"

const dbConfig = {
  host: process.env["DB_HOST"] || '127.0.0.1',
  port: Number(process.env['DB_PORT']) || 3306,
  user: process.env["DB_USER"] || 'no env',
  password: process.env["DB_PASS"] || 'no env',
  database: "todo_type"
};

const secret = process.env['SECRET'] || 'dEfAuLtSeCrEt1234'
export { _type, dbConfig, secret }