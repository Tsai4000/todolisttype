
import { Sequelize } from 'sequelize'
import { dbConfig, _type } from './config/config'


// Prod 時,關閉 query logging.
const sequelize: Sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.user,
  dbConfig.password,
  {
    host: dbConfig.host,
    port: dbConfig.port,
    dialect: "mysql",
    pool: {
      max: 10,
      min: 0,
      idle: 10000,
    },
    define: {
      charset: 'utf8',
      collate: 'utf8_general_ci',
      timestamps: false,
      freezeTableName: true
    },
    timezone: "+08:00",
    logging: _type === "Dev" ? (message) => { console.log(message) } : false
  }
)


sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch(err => {
    console.log('Unable to connect to the database:', err)
    process.exit()
  })



export { sequelize, }
