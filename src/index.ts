import { sequelize } from './dbconnection'
import { errorMiddleware } from './error/errorMiddleware'
import { socketServer } from './config/socket'
import { app } from './config/express'
import { io } from './socket'
import { routers } from './router'

const port = Number(process.env.SERVER_PORT || 3000)


routers.forEach(({ method, path, beforeAll, handlers, afterAll }) => {
  app[method](path, beforeAll, handlers, errorMiddleware, afterAll)
})

app.listen(port, '0.0.0.0', async () => {
  await sequelize.sync()
  console.log(`running on port ${port}`)
})

socketServer.listen(port)