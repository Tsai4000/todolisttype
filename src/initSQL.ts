import mysql from 'mysql2/promise'
import dotenv from 'dotenv'
dotenv.config()

const initialize = async () => {
  const connection = await mysql.createConnection({
    host: process.env.DB_HOST || '127.0.0.1',
    port: Number(process.env.DB_PORT) || 3306,
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASS || 'root'
  })

  await connection.query(`CREATE DATABASE IF NOT EXISTS ${process.env.DATABASE || 'todo_type'};`)
  process.exit()
}

initialize()