import { authValidation } from "../controllers/validation"
import { RequestHandler } from 'express'
import fetch from 'node-fetch'

const startupHandler: RequestHandler = async (req, res, next) => {
  try {
    await fetch(`http://localhost:3000/member`, {
      method: 'post',
      body: JSON.stringify({ account: 'test', passwd: 'test' }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(() => console.log('login with account: test, passwd: test'))
      .catch((err) => { throw err })
    res.render('login')
  } catch (err) {
    console.log(err)
    next(err)
  }
}

const renderWelcome: RequestHandler = (req, res, next) => {
  try {
    return res.render('welcome')
  } catch (err) {
    console.log(err)
    next(err)
  }
}

const elseRouter: router[] = [
  {
    method: 'get',
    path: '/',
    beforeAll: [],
    handlers: [startupHandler],
    afterAll: []
  }, {
    method: 'get',
    path: '/welcome',
    beforeAll: [authValidation],
    handlers: [renderWelcome],
    afterAll: []
  },
]

export { elseRouter }