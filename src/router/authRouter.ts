import { login } from "../controllers/memberCtrl"
import { bodyLoginValidate, validateResult } from "../controllers/validation"

const authRouter: router[] = [
  {
    method: 'post',
    path: '/auth',
    beforeAll: [],
    handlers: [...bodyLoginValidate, validateResult, login],
    afterAll: [],
  }
]

export { authRouter }