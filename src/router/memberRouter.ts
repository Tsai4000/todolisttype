import { RequestHandler } from 'express'
import { createUser } from '../controllers/memberCtrl'
import { bodyLoginValidate, validateResult } from '../controllers/validation'

interface postMemberBody {
  account: String,
  passwd: String
}

const postMemberValidation: RequestHandler = (req, res, next) => {
  try {
    const _: postMemberBody = req.body
    next()
  } catch (err) {
    next(err)
  }
}

const memberRouter: router[] = [
  {
    method: 'post',
    path: '/member',
    beforeAll: [],
    handlers: [...bodyLoginValidate, validateResult, createUser],
    afterAll: [],
  }
]

export { memberRouter }