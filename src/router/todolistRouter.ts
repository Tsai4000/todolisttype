import { RequestHandler } from "express"
import { renderTodoPage, showInitTodo, showTodoDetail, showTodoList, updateTodoDetail } from "../controllers/todoCtrl"
import { authValidation, paramsTodoIdValidate, validateResult } from "../controllers/validation"
import { todoUpdateEmitter } from "../socket/emitter"


const todoUpdateNotification: RequestHandler = (req, res, next) => {
  todoUpdateEmitter(req.body)
}

const todolistRouter: router[] = [
  {
    method: 'get',
    path: '/to-do-list/page',
    beforeAll: [authValidation],
    handlers: [renderTodoPage],
    afterAll: []
  }, {
    method: 'get',
    path: '/to-do-list/list',
    beforeAll: [authValidation],
    handlers: [showTodoList],
    afterAll: []
  }, {
    method: 'get',
    path: '/to-do-list/detail/create/page',
    beforeAll: [authValidation],
    handlers: [showInitTodo],
    afterAll: []
  }, {
    method: 'get',
    path: '/to-do-list/detail/:to_do_id',
    beforeAll: [authValidation],
    handlers: [paramsTodoIdValidate, validateResult, showTodoDetail],
    afterAll: []
  }, {
    method: 'put',
    path: '/to-do-list/detail/:to_do_id',
    beforeAll: [authValidation],
    handlers: [paramsTodoIdValidate, validateResult, updateTodoDetail],
    afterAll: [todoUpdateNotification]
  },
]

export { todolistRouter }