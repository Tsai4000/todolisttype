import { memberRouter } from "./memberRouter"
import { authRouter } from "./authRouter"
import { todolistRouter } from "./todolistRouter"
import { elseRouter } from "./elseRouter"

const routers = [
  ...memberRouter,
  ...authRouter,
  ...todolistRouter,
  ...elseRouter
]

export { routers }