import { findOne, findAll, createOne, updateOne } from '../models/todoModel'
import moment from 'moment'
import { BadRequest } from '../error/errors'

const findTodos = async () => {
  const todos = await findAll()
  return todos
}

const createInitTodo = async () => {
  const todo = await createOne()
  return todo['to_do_id']
}

const findSpecifyTodo = async (to_do_id: number) => {
  const todo = await findOne({ where: { to_do_id } })
  if (!todo) throw new BadRequest('Cannnot find todo.')
  todo.reserved_time = moment(todo.reserved_time).format('YYYY-MM-DD hh:mm')
  return todo
}

const modifySpecifyTodo = async (to_do_id: number, todo: Todo.updateValue) => {
  const update = await updateOne(todo, { where: { to_do_id } })
  return update == 1 ? 'ok.' : 'update failed'
}


export { findTodos, createInitTodo, findSpecifyTodo, modifySpecifyTodo }
