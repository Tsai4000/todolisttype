import * as bcrypt from 'bcrypt'
import * as MemberModel from '../models/memberModel'
import { Unauthorized } from '../error/errors'

const CreateMember = async (memberValue: Member.createValue) => {
  const insertResult = await MemberModel.createOne(memberValue)
  return insertResult
}

const Login = async (account: string, password: string) => {
  const member = await MemberModel.findOne({ where: { account } })
  if (member && bcrypt.hashSync(password, member.salt) === member.passwd) {
    return { account }
  } else {
    throw new Unauthorized('Login Failed')
  }
}

export {
  CreateMember,
  Login
}