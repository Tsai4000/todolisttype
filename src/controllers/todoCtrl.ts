import * as TodoAction from '../actions/todoAction'
import { RequestHandler } from 'express'

const renderTodoPage: RequestHandler = (req, res, next) => {
  try {
    return res.render('to-do-list')
  } catch (err) {
    console.log(err)
    next(err)
  }
}

const showTodoList: RequestHandler = async (req, res, next) => {
  try {
    const todos = await TodoAction.findTodos()
    res.status(200).json({ result: todos })
  } catch (err) {
    res.render('welcome')
  }
}

const showInitTodo: RequestHandler = async (req, res, next) => {
  try {
    const todoId = await TodoAction.createInitTodo()
    res.render('to-do-detail', { "to_do_id": todoId })
  } catch (err) {
    res.render('to-do-list')
  }
}

const showTodoDetail: RequestHandler = async (req, res, next) => {
  try {
    const todo = await TodoAction.findSpecifyTodo(Number(req.params.to_do_id))
    res.render('to-do-detail', todo)
  } catch (err) {
    res.render('to-do-list')
  }
}

const updateTodoDetail: RequestHandler = async (req, res, next) => {
  try {
    const todo = await TodoAction.modifySpecifyTodo(Number(req.params.to_do_id), req.body)
    res.status(200).json({ message: todo })
    if (todo === 'ok.') next()
  } catch (err) {
    res.render('to-do-list')
  }
}

export { showInitTodo, showTodoList, showTodoDetail, updateTodoDetail, renderTodoPage }