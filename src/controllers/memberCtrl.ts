import * as MemberAction from '../actions/memberAction'
import * as jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import { RequestHandler } from 'express'


const createUser: RequestHandler = async (req, res, next) => {
  try {
    const salt = await bcrypt.genSalt(10)
    const memberValue: Member.createValue = {
      account: req.body.account as string,
      passwd: bcrypt.hashSync(req.body.passwd, salt),
      salt
    }
    const member = await MemberAction.CreateMember(memberValue)
    return res.status(200).json({ account: member.account })
  } catch (err) {
    console.log(err)
    next(err)
  }
}

const login: RequestHandler = async (req, res, next) => {
  try {
    const member = await MemberAction.Login(req.body.account, req.body.passwd)
    res.cookie('token', jwt.sign(member, process.env.SECRET || '')).json({
      redirect: 'welcome',
      message: 'Login success'
    })
    next()
  } catch (err) {
    next(err)
  }
}

export { createUser, login }