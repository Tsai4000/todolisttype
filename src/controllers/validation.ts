import { RequestHandler } from "express"
import { body, param, validationResult } from "express-validator"
import * as jwt from 'jsonwebtoken'
import { secret } from "../config/config"

const paramsTodoIdValidate = param('to_do_id').exists().withMessage('to_do_id is required')

const bodyLoginValidate = [
  body('account').exists().withMessage('account is required'),
  body('passwd').exists().withMessage('password is required')
]

const validateResult: RequestHandler = (req, res, next) => {
  try {
    validationResult(req).throw()
    next()
  } catch (err) {
    next(err)
  }
}

const authValidation: RequestHandler = (req, res, next) => {
  const token = req.cookies.token
  if (token) {
    jwt.verify(token, secret, (err: Error | null, decoded: object | undefined) => {
      if (err) {
        return res.render('login')
      } else {
        req.body.decoded = decoded
        next()
      }
    })
  }
}

export { paramsTodoIdValidate, bodyLoginValidate, validateResult, authValidation }