import { socketServer } from "../config/socket";
import { Server, Socket } from "socket.io";
import { onConnection } from "./handler";
import * as jwt from 'jsonwebtoken'
import { secret } from "../config/config";
import { Unauthorized } from "../error/errors";


const socketSessionVerify = (socket: SocketDecodedBody, next: (err?: Error) => void) => {
  if (socket.handshake.query && socket.handshake.query.token && typeof socket.handshake.query.token === 'string') {
    jwt.verify(socket.handshake.query.token, secret, (err, decoded) => {
      if (err) return next(new Unauthorized('Authentication error'))
      socket.decoded = decoded
      next()
    })
  } else {
    next(new Error('Authentication error'))
  }
}
const io = new Server(socketServer, { cors: {} })

// io.use(socketSessionVerify)
io.on('connection', onConnection)

export { io }