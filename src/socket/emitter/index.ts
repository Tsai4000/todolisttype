import { io } from '../index'

const todoUpdateEmitter = (data: Todo.updateValue) => {
  io.emit('todoUpdate', data)
}

export { todoUpdateEmitter }