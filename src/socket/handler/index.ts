import { Socket } from 'socket.io'

const onConnection = (socket: Socket) => {
  console.log('conn', socket.id)
}

export { onConnection }