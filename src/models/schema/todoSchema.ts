import { Model, DataTypes } from 'sequelize'
import { sequelize } from '../../dbconnection'


export interface TodoCreationModel {
  to_do_id?: number
  subject?: string
  reserved_time?: string
  modified_time?: string
  brief?: string
  level?: string
  author?: string
  content?: string
}

export interface TodoModel extends Model<TodoModel, TodoCreationModel> {
  to_do_id: number
  subject: string
  reserved_time: string
  modified_time: string
  brief: string
  level: string
  author: string
  content: string
}


const todo = sequelize.define<TodoModel, TodoCreationModel>('Todo', {
  to_do_id: {
    primaryKey: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    unique: true,
    autoIncrement: true,
  },
  subject: { type: DataTypes.STRING, },
  reserved_time: { type: DataTypes.DATE },
  modified_time: { type: DataTypes.DATE },
  brief: { type: DataTypes.STRING },
  level: { type: DataTypes.INTEGER },
  author: { type: DataTypes.STRING },
  content: { type: DataTypes.STRING },
}, {
  tableName: 'todos'
});


export { todo }