import { Model, DataTypes } from 'sequelize'
import { sequelize } from '../../dbconnection'

export interface MemberCreationModel {
  account: string
  passwd: string
  salt: string
}

export interface MemberModel extends Model<MemberModel, MemberCreationModel> {
  account: string
  passwd: string
  salt: string
}


const testMember = sequelize.define<MemberModel, MemberCreationModel>('testMember', {
  account: {
    primaryKey: true,
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    validate: {}
  },
  passwd: { type: DataTypes.STRING, allowNull: false },
  salt: { type: DataTypes.STRING, allowNull: false }
}, {
  tableName: 'members'
});


export { testMember }