import { todo } from './schema/todoSchema'

interface whereCondition {
  where: {
    to_do_id: number
  }
}

const createOne = (createVale?: Todo.createValue) => {
  return todo.create(createVale)
}

const updateOne = async (upValue: Todo.updateValue, where: whereCondition) => {
  const result = await todo.update(upValue, where)
  return result[0]
}

const findOne = async (where: whereCondition) => {
  const result = await todo.findOne({ ...where, raw: true })
  return result
}

const findAll = async () => {
  const results = await todo.findAll({ raw: true })
  return results
}

export { createOne, updateOne, findOne, findAll }