import { testMember as tm } from './schema/memberSchema'

interface whereCondition {
  where: {
    account: string
  }
}

const createOne = (createValue: Member.createValue) => {
  return tm.create(createValue)
}

const testCreateOne = (createVale: Member.createValue) => {
  return tm.create(createVale)
}

const updateOne = async (upValue: Member.updateValue, where: whereCondition) => {
  const result = await tm.update(upValue, where)
  return result[0]
}
// const co = async () => {
//   const inputValue = {
//     account: 'tacc', passwd: 'tp', salt: 's'
//   }
//   await createOne(inputValue)
//   await testCreateOne(inputValue)
// }

const findOne = async (where: whereCondition) => {
  const result = await tm.findOne(where)
  return result
}

export { createOne, testCreateOne, updateOne, findOne }